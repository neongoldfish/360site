<?php
    $string = get_term(get_nav_menu_locations()['footer3'], 'nav_menu')->name;
    if ($string) {
        echo '<div class="menu footer3"><div class="title">'.$string.'</div>';
    
          wp_nav_menu( array(
            'theme_location'		=> 'footer3',
            'container'         => false,

          ));
        echo '</div>';
    }
?>