    
<?php
    $string = get_term(get_nav_menu_locations()['footer2'], 'nav_menu')->name;
    if ($string) {
        echo '<div class="menu footer2"><div class="title">'.$string.'</div>';
    
          wp_nav_menu( array(
            'theme_location'		=> 'footer2',
            'container'         => false,

          ));
        echo '</div>';
    }
?>