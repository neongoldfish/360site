<!--Schema Markup See http://google.schema.org/LocalBusiness for more details and syntax-->
<?php global $theme_vars;?> 

<div class= "slb" itemscope itemtype="http://schema.org/LocalBusiness">
    <?php 
        $logo = get_field ('footer_logo', 'option');
        if ($logo != "") {
            
            
            echo '<img class="logo" itemprop="image" src="'.wp_get_attachment_url($logo).'" alt="'.get_bloginfo('name').'">';
            
        }
        else {
            echo get_bloginfo('name');
        }
    ?>
    <span class="name sr-only" itemprop="name"><?php echo $theme_vars['Company Name'];?></span><br>
    <p class="address" itemprop="address"><?php echo $theme_vars['Street Address'].' <br>'.$theme_vars['City'].', '.$theme_vars['State'].' '.$theme_vars['Zip'];?></p>
    <a title="click to call" class="phone" itemprop="telephone" rel="nofollow" href="tel: +1<?php echo preg_replace("/[^0-9]/","",$theme_vars['phone']);?>"><?php echo $theme_vars['phone'];?></a>
    <p class="hours" itemprop="openingHours" content="<?php echo $theme_vars['Hours Schema'];?>"><?php echo $theme_vars['Hours Visual'];?></p>
</div>

<!--End Schema Markukp-->