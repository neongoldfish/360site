
    
<?php
    $string = get_term(get_nav_menu_locations()['footer1'], 'nav_menu')->name;
    if ($string) {
        echo '<div class="menu footer1"><div class="title">'.$string.'</div>';
    
          wp_nav_menu( array(
              'theme_location'		=> 'footer1',
              'container'         => false,

          ));
        echo '</div>';
    }
?>
