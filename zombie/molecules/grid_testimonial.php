<div class="testimonial">
    <div class="content">
        <?php the_content();?>
    </div>
    <div class="signature"><?php the_title();?></div>
</div>