<style>
    .social.fontawesome a {
        text-decoration: none;
        display: inline-block;
        vertical-align: middle;
        margin-left: 5px;
        font-size: 1.25em;
    }
    .social.fontawesome a i{
       
    }
</style>
<div class="social fontawesome">				
    <a class="youtube" href="#"> 
        <i class="fab fa-youtube"></i>
        <span class="sr-only">View Our Youtube Page</span>
    </a>			               
    <a class="instagram" href="#">
        <i class="fab fa-instagram"></i>
        <span class="sr-only">View our Instagram Images</span>
    </a>
    
    <a class="pinterest" href="#">
        <i class="fab fa-pinterest-p"></i>
        <span class="sr-only">View our Pinterest page</span>
    </a>
    
    <a class="vimeo" href="#">
        <i class="fab fa-vimeo-v"></i>
        <span class="sr-only">View our Vimeo Videos</span>
    </a>
    
    <a class="tumblr" href="#">
        <i class="fab fa-tumblr"></i>
        <span class="sr-only">View our Tumblr page</span>
    </a>

    <a class="facebook" href="#">
        <i class="fab fa-facebook"></i>
        <span class="sr-only">Follow us Facebook</span>
    </a>
    <a class="twitter"  href="#">
        <i class="fab fa-twitter"></i>
        <span class="sr-only">Follow us On Twitter</span>
    </a>
    <a class="linkedin"  href="#">
        <i class="fab fa-linkedin"></i>
        <span class="sr-only">View our LinkedIn Profile</span>
    </a>    
    <a class="blog"  href="#">
        <i class="fas fa-rss"></i>
        <span class="sr-only">Visit our Blog</span>
    </a>
</div>