<!-- Begin partial -->
<head>
    <meta charset="utf-8">
    <title>{{ content.html_title }}</title>
    <meta name="description" content="{{ content.meta_description }}">
    {{ standard_header_includes }}
</head>
<a href="#content" title="Skip Navigation" href="#content" class="sr-only sr-only-focusable">Skip to main content</a>
<header id="header">
    <div class="top text-center">
        <div class="container text-md-right">
            <a href="https://schedule360.com/tutorials/">Tutorials</a>
        </div>
    </div>
    <div class="bottom">
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-4 text-md-left">
              {% logo "logo" %}
            </div>
            <div class="col-lg-8 text-md-right">
                <div id="login">
                    <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-7e2ed5ad-f3b1-458f-9eb1-de8c8cb8e838"><span class="hs-cta-node hs-cta-7e2ed5ad-f3b1-458f-9eb1-de8c8cb8e838" id="hs-cta-7e2ed5ad-f3b1-458f-9eb1-de8c8cb8e838" data-hs-drop="true" style="visibility: visible;"><a id="cta_button_5815441_7349d033-6620-403c-897f-0049d5ab9351" class="cta_button " href="https://inbound.schedule360.com/cs/c/?cta_guid=7349d033-6620-403c-897f-0049d5ab9351&amp;placement_guid=7e2ed5ad-f3b1-458f-9eb1-de8c8cb8e838&amp;portal_id=5815441&amp;canon=https%3A%2F%2Fschedule360.com%2F&amp;redirect_url=APefjpGC4Wl9xuDkGXLa8aq9xKoi-NF9BL0n2S_ewn9UidGckWNdLkwh0dXTJlFZeNtQX4LvTy6Mim_zoZNUOIyy35ZPXu6FfAOercSw4M72OM8u6gvZObmhCgCdcG5Kvj0POjuroZN0OSLFW3Zi9WwbAMrTQAGC4h1Ew_FpJH7EK5uf2AbmMmjSKS8qhGs0M0PwcqX-Kq_DoFfczbGGeUEICdmR0AIFcod0JI9KaNCh9wCmlmj0AUOISLO4olZ0C87n8HRoG82XO3z0vXEorARiYkqQtovBYg&amp;click=8e476f7c-1620-45ff-b047-e53c3188c48d&amp;hsutk=b76520fd08436b96fedc14e05ac5286b&amp;__hstc=157941989.b76520fd08436b96fedc14e05ac5286b.1559573944871.1559830512460.1559921096105.9&amp;__hssc=157941989.3.1559921096105&amp;__hsfp=1885817573" style="" cta_dest_link="https://apps.schedule360.com/sart" title="Login"><span style="color: #ffffff;"><strong>Login</strong></span></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(5815441, '7e2ed5ad-f3b1-458f-9eb1-de8c8cb8e838', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
                    <br>
                    <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-f877074c-5e5d-4904-8d01-ff7230efd9bc"><span class="hs-cta-node hs-cta-f877074c-5e5d-4904-8d01-ff7230efd9bc" id="hs-cta-f877074c-5e5d-4904-8d01-ff7230efd9bc" data-hs-drop="true" style="visibility: visible;"><a id="cta_button_5815441_3f0b33bd-3f1e-47fc-9c88-e1da133e1f68" class="cta_button " href="https://inbound.schedule360.com/cs/c/?cta_guid=3f0b33bd-3f1e-47fc-9c88-e1da133e1f68&amp;placement_guid=f877074c-5e5d-4904-8d01-ff7230efd9bc&amp;portal_id=5815441&amp;canon=https%3A%2F%2Fschedule360.com%2F&amp;redirect_url=APefjpGM0waeOAKaHoxRvYiuoo4EO5wxVZUVyHfuRM6_sXLDAP4T3QsEqj3RvYq8NoG1JjpLL9MmyGHJkqynuAk8WL7IkA-m8FSbq3v3wicYy9jAjwJ7NQfSmnMuZWFN83Xdn4C3XC8l7wPiDrHv6k0vHzTWOIOXezpyNQH27rtAVJPkDv7lVHIY0SKkSNk-2oC-yKcbkRSorA7NzQHiEubsp6Osy69acogU-AE6eMHFMegv5x_A5_BV57-BN7DaGNKwe2gyHnqH&amp;click=bd3e7359-4b68-478e-afc5-f6e905648d1c&amp;hsutk=b76520fd08436b96fedc14e05ac5286b&amp;__hstc=157941989.b76520fd08436b96fedc14e05ac5286b.1559573944871.1559830512460.1559921096105.9&amp;__hssc=157941989.3.1559921096105&amp;__hsfp=1885817573" style="" cta_dest_link="https://schedule360.com/m/" title="Mobile Login"><span style="color: #ffffff;"><strong>Mobile Login</strong></span></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(5815441, 'f877074c-5e5d-4904-8d01-ff7230efd9bc', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
                    <br>
                    <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-95bb752e-3dda-4b64-8ef9-fab50fb52344"><span class="hs-cta-node hs-cta-95bb752e-3dda-4b64-8ef9-fab50fb52344" id="hs-cta-95bb752e-3dda-4b64-8ef9-fab50fb52344" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_5815441_33aac2f7-39b9-4c72-8cde-d74efe66ca66" class="cta_button " href="https://inbound.schedule360.com/cs/c/?cta_guid=33aac2f7-39b9-4c72-8cde-d74efe66ca66&amp;placement_guid=95bb752e-3dda-4b64-8ef9-fab50fb52344&amp;portal_id=5815441&amp;canon=https%3A%2F%2Fschedule360.com%2F&amp;redirect_url=APefjpFT0Jpz0yTW2TGMM-S93Jh1z4-G4bmLifA_4FmqA3-2jUPCSsBaIpshDxrFZp6YRBSMO0nPTUIXErBKz9tgl4Ho2EqhZU77i_UrqsTco-4izIPc3B8qOUU92sDIPPt3l5q7tq5UO7Rk_DlZP3sVwFlrBP-0Ej0xPZ2UWM9CHCm2TqkjGRWsKdmnWeo_elE8xbEEbgS-NVROq3sAN6AXZ_rRtVpbR86P22IdzVZe2FLWqwFHUDNvvbTmpB_y6CnFE1T3TYh4t6-KLJL32vkJz4j8HB2wzlRaKkaQARuf-Tq2DaogelXtfrUPqvr23h-a6fWzi1G4&amp;click=581cadb0-ee89-4d02-b2d7-5f666308e3ef&amp;hsutk=b76520fd08436b96fedc14e05ac5286b&amp;__hstc=157941989.b76520fd08436b96fedc14e05ac5286b.1559573944871.1559830512460.1559921096105.9&amp;__hssc=157941989.3.1559921096105&amp;__hsfp=1885817573" style="" cta_dest_link="https://apps.schedule360.com/yart/paccess.sub_forget_pwd_pkg.submit_username" title="Lost Password?"><strong><span style="color: #2785ad;">Lost Password?</span></strong></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(5815441, '95bb752e-3dda-4b64-8ef9-fab50fb52344', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
                    
                </div>
                <div id="navigation">
                    <div class="container">
                        <nav class="navbar navbar-toggleable-md">

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>Menu 
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                  <ul id="primaryNav" class="navbar-nav"><li id="menu-item-9" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item menu-item-9"><a href="https://schedule360.com/">Home</a></li>
                                    <li id="menu-item-10" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-10"><a href="https://schedule360.com/about/">About</a>
                                    <ul class="dropdown-menu">
                                        <li id="menu-item-15" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15"><a href="https://schedule360.com/about/our-difference/">:: Our Difference</a></li>
                                        <li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="https://schedule360.com/about/technology-overview/">:: Technology Overview</a></li>
                                        <li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="https://schedule360.com/about/clients/">:: Clients</a></li>
                                        <li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="https://schedule360.com/about/success-stories/">:: Success Stories</a></li>
                                        <li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="https://schedule360.com/about/partners/">:: Partners</a></li>
                                        <li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30"><a href="https://schedule360.com/about/customer-support/">:: Customer Support</a></li>
                                        <li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="https://schedule360.com/about/getting-started/">:: Getting Started</a></li>
                                    </ul>
                                    </li>
                                    <li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="https://schedule360.com/features/">Features</a></li>
                                    <li id="menu-item-46" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-46"><a href="https://schedule360.com/solutions/">Solutions</a>
                                    <ul class="dropdown-menu">
                                        <li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49"><a href="https://schedule360.com/solutions/hospitals/">:: Hospitals</a></li>
                                        <li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55"><a href="https://schedule360.com/solutions/physician-group/">:: Physician Group Practices</a></li>
                                        <li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a href="https://schedule360.com/solutions/pharmaceuticaldevice/">:: Pharmaceutical/Device</a></li>
                                        <li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a href="https://schedule360.com/solutions/telecom-call-centers/">:: Telecom Call Centers</a></li>
                                        <li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a href="https://schedule360.com/solutions/disaster-planning/">:: Disaster Planning</a></li>
                                        <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><a href="https://schedule360.com/solutions/ipop-clinics/">:: IP/OP Clinics</a></li>
                                        <li id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70"><a href="https://schedule360.com/solutions/hospitalists/">:: Hospitalists</a></li>
                                        <li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="https://schedule360.com/solutions/pharmacies/">:: Chain Pharmacy</a></li>
                                        <li id="menu-item-1151" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1151"><a href="https://schedule360.com/solutions/float-pool-scheduling/">:: Float Pool Scheduling</a></li>
                                        <li id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a href="https://schedule360.com/solutions/on-demand-scheduling/">:: On-Demand Scheduling</a></li>
                                        <li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="https://schedule360.com/solutions/government/">:: Government</a></li>
                                        <li id="menu-item-1216" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1216"><a href="https://schedule360.com/solutions/payroll-based-journal/">:: Payroll Based Journal</a></li>
                                    </ul>
                                    </li>
                                    <li id="menu-item-190" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-190"><a href="https://schedule360.com/faqs/">FAQs</a></li>
                                    <li id="menu-item-238" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-238"><a href="https://schedule360.com/category/news/">News</a>
                                    <ul class="dropdown-menu">
                                        <li id="menu-item-237" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-237"><a href="https://schedule360.com/category/articles/">:: Articles</a></li>
                                        <li id="menu-item-247" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-247"><a href="https://schedule360.com/category/blog/">:: Blog</a></li>
                                    </ul>
                                    </li>
                                    <li id="menu-item-44" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44"><a href="https://schedule360.com/contact/">Contact</a></li>
                                    </ul>
                            </div>

                        </nav>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </div>
     
</header>
<!-- End partial -->