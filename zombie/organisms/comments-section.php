<style>
    .commentlist .comment .comment {
        padding-left: 1em;
    }
</style>
<div id="comments_section">
     <div class="container">
         <h2>Comments</h2>
         <?php comments_template( '/comments.php' ); ?> 
    </div>
</div>