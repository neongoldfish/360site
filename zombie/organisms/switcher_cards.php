<div class="cardwrap col-sm-4">
    <div class="acard">
        <div class="title"><?php echo $vars['Title'];?></div>
        <div class="row">
            <div class="col-sm-12 image">
                <div class="img"><img class="featured-img" src="<?php echo $vars['Image'];?>" alt="<?php echo $vars['ImageAlt'];?>"></div>
            </div>
            <div class="col-sm-12 thecontent">
                <div class="content"><?php echo $vars['Content'];?></div>
            </div>
        </div>
    </div>
</div>