<?php //Single Column Text
    global $post;
?>
    <div id="<?php echo get_sub_field('css_id') ;?>" class="page_section custom_section <?php echo get_sub_field('classes');?>"> 
		<?php get_atomic_part('/templates/custom-'.get_sub_field('template_name').'.php', 0);?>
	</div> 