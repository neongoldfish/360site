<div id="default_content">
    <?php get_atomic_part ('molecules/page_title.php', $args);?>
    <div class="container main-content">
        
		<?php 
		if ($args['content'] !="") {
			echo apply_filters( 'the_content', $args['content']); 
		}
		get_atomic_part ('/inc/page_builder.php', $args);
		?>
          
          
    </div>
	<?php 
	$frontpage_id = get_option( 'page_on_front' );
		clone_layout($frontpage_id, 'tradeschool-callout');
		
	?>
</div>