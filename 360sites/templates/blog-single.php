<?php
//Design Library Name: Blog
//Description: This is the Prototype Implimentation of the main Blog Feed  It includes a loop of all blog posts and a fully widgetized sidebar.
?>
<!-- Begin Template: Blog -->
<div id="template-blog">
   <div class="container main-content">
        <div class="row">
            <div class="col-sm-12 col-lg-9 main">
				<h1><?php the_title();?></h1>
                <?php 
                if ($args['content'] !="") {
                    echo apply_filters( 'the_content', $args['content']); 
                }
                get_atomic_part ('/inc/page_builder.php', $args);
                ?>
            </div>
            <div class="col-lg-3 sidebar">
                <?php get_sidebar('page');?>
            </div>
        </div>
    </div>
            
        
</div>
<!-- End Template: Blog -->