<div id="default_content_sidebar">
    
    <div class="container main-content">
        
        
		<h1><?php the_title();?></h1>
		<?php 
		if ($args['content'] !="") {
			echo apply_filters( 'the_content', $args['content']); 
		}
		get_atomic_part ('/inc/page_builder.php', $args);
		?>
        
    </div>
</div>
<?php 
	$frontpage_id = get_option( 'page_on_front' );
	clone_layout($frontpage_id, 'tradeschool-callout');

	
?>