<?php 

require get_stylesheet_directory() . '/inc/post_type-testimonial.php';
require get_stylesheet_directory() . '/inc/post_type-project.php';

//function to get the JSON
function ngfGetPosts($limit, $offset) { //Limit = number of blogposts to fetch  //offset = number of posts to offset from the start of the list 
    $hapikey= '853a200b-dc8e-47b4-b297-f7ed8ebd2edc'; //hubspot API Key
    $guid = '9156379132'; //blog unique identifier available at https://app.hubspot.com/blog/553762/dashboard/xxxxxx
    $url = 'https://api.hubapi.com/content/api/v2/blog-posts?hapikey='.$hapikey.'&content_group_id='.$guid.'&limit='.$limit.'&offset='.$offset.'&order_by=-publish_date&state=PUBLISHED';
    
    //GET the Data From the URL
    $json = file_get_contents($url);
    //Turn the Data Into a php object
    $object = json_decode($json,true);
    $output = ($object['objects']);
    return $output;
}
function ngfGetSlug ($id) { //Limit = number of blogposts to fetch  //offset = number of posts to offset from the start of the list
    //$hapikey= '5ed6df16-86be-446a-9a16-64d2bdfc45e0'; //hubspot API Key
	$hapikey= '853a200b-dc8e-47b4-b297-f7ed8ebd2edc'; //hubspot API Key
    $url = 'https://api.hubapi.com/blogs/v3/topics/'.$id.'?hapikey='.$hapikey;
	
    //GET the Data From the URL
    $json = file_get_contents($url);
    //Turn the Data Into a php object
    $object = json_decode($json,true);
    $output = ($object['slug']);
    return $output;
}