<!--Schema Markup See http://google.schema.org/LocalBusiness for more details and syntax-->
<?php global $theme_vars;?> 

<div class= "slb" itemscope itemtype="http://schema.org/LocalBusiness">
    <span class="address" itemprop="address"><?php echo $theme_vars['Street Address'].', '.$theme_vars['City'].', '.$theme_vars['State'].' '.$theme_vars['Zip'];?></span>
</div>

<!--End Schema Markukp-->