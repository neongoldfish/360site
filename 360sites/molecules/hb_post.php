
<?php
    if (has_post_thumbnail()) {
        $theurl = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
    }
else {  
    $content = get_the_content();
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $content, $matches);
        if (isset($matches[1])) {
            $theurl = $matches[1];
        } else {
            $theurl = get_stylesheet_directory_uri().'/img/logo.svg';
            $class = "noimage";
        }
    } 
?>
<a class="postwrap" href="<?php echo $vars['absolute_url'];?>">
    <div class="post" style="background-image:url(<?php echo $vars['post_list_summary_featured_image'];?>)">
		<div class="overlay">

			<h3><?php echo $vars['html_title'];?></h3>
			<p>
				<?php
				$regex = '#(<h([1-6])[^>]*>)\s?(.*)?\s?(<\/h\2>)#';
				$excerpt = $vars['post_body'];
				$excerpt = preg_replace($regex,'', $excerpt);
				$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
				$excerpt = strip_shortcodes($excerpt);
				$excerpt = strip_tags($excerpt);
				$excerpt = substr($excerpt, 0, 100);
				$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
				$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));

				echo $excerpt;
				?>
			</p>
			<span class="btn learnmore">Learn more</span>

		</div>
	</div>
</a>