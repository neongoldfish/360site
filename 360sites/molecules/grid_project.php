
<?php
    if (has_post_thumbnail()) {
        $theurl = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
    }
else {  
    $content = get_the_content();
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $content, $matches);
        if (isset($matches[1])) {
            $theurl = $matches[1];
        } else {
            $theurl = get_stylesheet_directory_uri().'/img/logo.svg';
            $class = "noimage";
        }
    } 
?>

    <a class="slide" href="<?php echo get_permalink();?>">
        
        <div class="image <?php echo $class;?>" style="background-image:url(<?php echo $theurl;?>)">
            <div class="overlay">
                <h3><?php the_title();?></h3>
                <div class="excerpt">
                <?php
                    $regex = '#(<h([1-6])[^>]*>)\s?(.*)?\s?(<\/h\2>)#';
                    $excerpt = get_the_content();
                    $excerpt = preg_replace($regex,'', $excerpt);
                    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
                    $excerpt = strip_shortcodes($excerpt);
                    $excerpt = strip_tags($excerpt);
                    $excerpt = substr($excerpt, 0, 125);
                    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
                    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
                    
                    echo $excerpt;
                ?>
                </div>
                <span class="readmore">View Project &raquo;</span>
            </div>
        </div>
    </a>
