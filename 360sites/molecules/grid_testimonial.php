<div class="testimonial">
	<div class="inner">
	<?php
	global $post;
    if (has_post_thumbnail()) {
        echo '<img src="'.wp_get_attachment_url(get_post_thumbnail_id($post->ID)).'" alt="Photo of Testmonial subject">';
    }
	?>
    
        <?php the_content();?>
    
	</div>
</div>