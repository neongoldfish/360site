<style>
    .social.fontawesome a {
        text-decoration: none;
        display: inline-block;
        vertical-align: middle;
        margin-left: 5px;
        font-size: 1.125em;
    }
    .social.fontawesome a i{
       
    }
</style>

<div class="social fontawesome">				
   
    <a class="facebook" href="https://www.facebook.com/pages/Schedule360/667166143323692">
        <i class="fab fa-facebook"></i>
        <span class="sr-only">Follow us Facebook</span>
    </a>
    <a class="twitter"  href="https://twitter.com/Schedule360">
        <i class="fab fa-twitter"></i>
        <span class="sr-only">Follow us On Twitter</span>
    </a>
    <a class="linkedin"  href="https://www.linkedin.com/company/schedule360/">
        <i class="fab fa-linkedin"></i>
        <span class="sr-only">View our LinkedIn Profile</span>
    </a>    
    
</div>