<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/unified_theme.js"></script>
<script type="text/javascript">
    var defaults = {
        controls:true,
        pager:false,
        auto:true,
        nextText:'<i class="fas fa-chevron-right"></i><span class="sr-only">Next</span>',
        prevText:'<i class="fas fa-chevron-left"></i><span class="sr-only">Previous</span>',
    };
    testiSlider = $("#tradeschool-testimonials .slider").bxSlider(defaults);
    
    function checkScreenSize(){
        

        var newWindowWidth = window.innerWidth;
        //console.log(newWindowWidth);
        if (newWindowWidth <= 992) {
            
            if (testiSlider.length !== 0) {
                testiSlider.reloadSlider(defaults);
            }
        }
        else {
            
            if (testiSlider.length !== 0) {
                testiSlider.destroySlider();
            }
            
        }
        
    };
        
    $(document).ready(function(){
        
        $('.hero_slider').bxSlider({
            auto:true,
            controls:true,
        });

        
        $(window).on("resize", function (e) {
            checkScreenSize();
        });
       
    });
	$(window).on("load", function (e) {
		checkScreenSize();
	});
</script>

<!--wordpress footer-->
<?php wp_footer(); ?> 