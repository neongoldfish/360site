<div class="col-md-4">
    <a href="<?php echo $vars['FeatureLink'];?>">
        <div class="block-feature" style="background-image:url(<?php echo $vars['BlockImage'];?>);">
			
            <div class="title">
				<div class="inner"><?php echo $vars['BlockTitle'];?></div>
				<div class="content"><?php echo $vars['BlockContent'];?></div>
			</div>
        </div>
    </a>
</div>