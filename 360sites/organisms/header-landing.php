<?php 
//Design Library Name: Header
//Description: This is the prototype implimentation of the page header.  It includes a logo, information area and navigation
?>
        <?php the_field('gtm_noscript', 'option');?>
        
		<a title="Skip Navigation" href="#content" class="sr-only sr-only-focusable">Skip to main content</a>
        
        <header> 
            <div id="header" class="landing">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-lg-3 text-lg-left">
                            <?php get_atomic_part ('/molecules/logo.php', 0);?>
                        </div>
                        <div class="col-lg-9 text-lg-right">
                            <div class="cta">Get your questions answered today! <?php get_atomic_part ('/molecules/phone.php', 0);?></div>
                        	
						</div>
                    </div>
                </div>
            </div>
        </header>
