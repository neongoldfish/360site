<div class="cardwrap col-sm-12">
    <?php if ($vars['CardLink'] != "") { ?>
        <a title="<?php echo $vars['CardTitle'];?>" href="<?php echo $vars['CardLink'];?>">
    <?php } ?>
        <div class="iconcard">
			<span class="sr-only"><?php echo $vars['CardTitle'];?></span>
            <div class="image" style="background-image:url(<?php echo $vars['CardImage'];?>)"></div>
        </div>
    <?php if ($vars['CardLink'] != "") { ?>
        </a>
    <?php } ?>
</div>