<?php
global $theme_vars;
    $theme_vars = array (
        'header_logo' => get_field ('logo', 'option'),
        'footer_logo' => get_template_directory_uri().'/img/logo.png',
        'Company Name' => get_field ('company_name', 'option'),
        'Street Address' => get_field ('co_street_address', 'option'),
        'City' => get_field ('co_city', 'option'),
        'State' => get_field ('co_state', 'option'),
        'Zip' => get_field ('co_zip', 'option'),
        'phone' => get_field ('co_phone', 'option'),
    );
?>