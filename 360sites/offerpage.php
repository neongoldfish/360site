<?php 
    if ($_GET['unzip'] != 'yes') {
        ob_start("sanitize_output");
    }
;?>
<?php 
    /**
     * Template Name: Offer Landing Page
     */
while (have_posts()) {
    the_post();
    $args = array (
        'pageTitle' => get_the_title(),
        'content'   => get_the_content(),
        'hero-image' => wp_get_attachment_url(get_post_thumbnail_id($post->ID)),
    );
    
?>
<?php 

?>
<!DOCTYPE html>

<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>     <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>     <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
        <?php get_atomic_part ('/meta/common_header.php');?>
		
	</head>
    <body <?php body_class(); ?>>
        <?php get_atomic_part('/organisms/header-landing.php', $args);?>
        <div id="content">
            <div id="landing">
				
				<?php get_atomic_part ('molecules/page_title.php', $args);?>
				<div class="container main-content">
					
							<?php 
							if ($args['content'] !="") {
								echo apply_filters( 'the_content', $args['content']); 
							}
							get_atomic_part ('/inc/page_builder.php', $args);
							?>
					
				</div>
			</div>
        </div>
        <?php get_atomic_part ('/molecules/copyright.php', 0);?>
        <?php get_atomic_part ('/meta/common_footer.php', 0);?>
    </body>
<?php } //endwhile?>    
</html>